import generated.asn1.SignedDirectEncryptedIdentityv2;
import org.junit.jupiter.api.Test;

import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


/**
 * Sanity check decoding/encoding binary encoding of the ASN.1-structures.
 */
class SignedDirectEncryptedIdentityTest {

    @Test
    void extraElementsIdempotentReencoding() {
        assertIdempotentReencoding("MIICPgYLYIQQAYdrCgECCgIwggHLMIIBVwYLYIQQAYdrCgECCQICAQECAQEWFDk5MDAwMDAxMjMxMjM0NTY3ODkwFhQ5OTk5MDAyMDAwMDAwMDAwMDAwMgIEATQXfzCB+QRRBBpjW+tcDbByOUY0Rr+lvNgt/Afq+ull626leQGXWDzToZYeqOjw7MZYbyp8ethgEd9q3aP5ty0vwCaKs2T7srO+V8WY6th1Paxjok7lk8YcBFEELaI6YNgch1PmKurg6GTSnvO9+MHe4K93Q4FJqQXJYw+yOmeXN78z6CceqEAsCOYpc94B/rcCK0XGRPq/Am9bQzMrna4tv/3KDqGs4owCIFIEUQRghvqwSLm8pNI18foR8iapjGhWIb6SxetiRd6a0iPdDcgCrMdEtd3ac3RP1hcUDrQ+04/f3cIWh1KA35GlTm6AbLy9mijcTuscmco+6MXmBYEUOTk5OTAwNDAwMDAwMDAwMDAwMDEEELNm8QS4RsqO8/J5/+6PWWECAQEWCDIwMTkwOTAxok8wDhYHVEFHMDFfSQIDAeJAMCEWB1RBRzAxX1MMFlRoZSBRdWljayBCcm93biBGb3guLi4wGhYHVEFHMDFfQgQPAQIDBAUGBwgJCgsMDQ4PMGAGCCqGSM49BAMDMFQCKF25kkCUDXgpnuMnIFWM3NSMRVp9/wi/2kLYGvp8P5WhO2Flqk7Dc8MCKAjPNneiECX0y1OijqrxBs1asnG9Dk8H7Rl8JKR+wqIv+5HBCy3TiPY=");
    }

    @Test
    void noEtraElementsIdempotentReencoding() {
        assertIdempotentReencoding("MIIB7wYLYIQQAYdrCgECCgIwggF6MIIBVwYLYIQQAYdrCgECCQICAQECAQEWFDk5MDAwMDAxMjMxMjM0NTY3ODkwFhQ5OTk5MDAyMDAwMDAwMDAwMDAwMQIEATPtxDCB+QRRBA/H0DMqmLFrU+qP/+wThiHGnmYt1OY8PEXbPO5Ds+JJJPJhbF9HX8Z+1pAAOhM+kjpXLAcm+9wi3kyEvc+TmZWPhNZcJ4BdPeUAjm6i5GMQBFEEuRIxeenGiWWDqbPK6iOhzxN7DBrrdV4FugcU2ILUft7YU52BfGEVVmxfQSTKr8Zm8ILQjMLP4y0MWrsGr4CC6hQjVBarqWkXOnoJiIoQb/oEUQQwNecVsEcX8Eix4i203cq+UcO/NSxC77k9HwogaaX4haj2rkWKUTevHD3LVymZVoXJ65DAeUgyLmLmJ4QLClikDnrMSO7YaqQo3d+wztXqb4EUOTk5OTAwNDAwMDAwMDAwMDAwMDEEEAIcEZ9nZbMmV45tRHQyQtECAQEWCDIwMTkwOTAxMGIGCCqGSM49BAMDMFYCKQC2JlHuhloJGtmF3FUNzMlbIwSClRzpcwDqlCsFCSN9qzylbtGXXkzfAikAxLEsQILjz5A1tCxVdHeVq4BYpbbzMBYNtIk/ir902U4Kp6LdNWTgEw==");
    }

    private void assertIdempotentReencoding(String base64) {
        // decode the base64 into the binding
        var bytes = Base64.getDecoder().decode(base64);
        var target = new SignedDirectEncryptedIdentityv2();
        assertSame(target, target.decodeByteArray(bytes));
        assertSame(target, target.decodeByteArray(bytes));

        // encode it using the binding
        var expectedEncoded = target.encodeByteArray();
        var encoded = target.encodeByteArray();
        assertArrayEquals(expectedEncoded, encoded);

        // decode the encoded form from the binding
        var redecoded = new SignedDirectEncryptedIdentityv2();
        assertSame(redecoded, redecoded.decodeByteArray(encoded));
        assertSame(redecoded, redecoded.decodeByteArray(encoded));

        var expectedReencoded = redecoded.encodeBase64();
        var reencoded = redecoded.encodeBase64();
        assertEquals(expectedReencoded, reencoded);

        // this should be identical to the input
        assertEquals(base64, reencoded);
    }

}
